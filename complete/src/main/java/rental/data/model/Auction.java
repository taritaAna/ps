package rental.data.model;

import humanize.Humanize;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.hibernate.annotations.FetchMode.JOIN;

/**
 * @author Tarita Ana
 */
@Entity
@Table(name = "Auction")
public class Auction {
    @Id
    @Column(name = "a_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    @OneToMany(mappedBy = "auction", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(JOIN)
    private List<Bid> biddings = new ArrayList<>();

    private Date endDate;

    private boolean awarded;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public List<Bid> getBiddings() {
        return biddings;
    }

    public void setBiddings(List<Bid> biddings) {
        this.biddings = biddings;
    }

    public boolean isAwarded() {
        return awarded;
    }

    public void setAwarded(boolean awarded) {
        this.awarded = awarded;
    }

    @Transient
    public String getTimeLeft() {
        return Humanize.duration(Duration.between(Instant.now(), getEndDate().toInstant()).toMillis() / 1000);
    }

    @Transient
    public Bid getLastBid() {
        if (getBiddings() == null || getBiddings().isEmpty()) {
            return null;
        }
        int last = getBiddings().size() - 1;
        return getBiddings().get(last);
    }

    @Transient
    public double getHighestBid() {
        if (getLastBid() == null) {
            return getCar().getRentalPrice();
        }
        return getLastBid().getBidAmount();
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "car");
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "car");
    }

}
