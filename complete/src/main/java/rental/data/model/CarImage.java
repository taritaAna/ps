package rental.data.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import javax.persistence.*;

/**
 * @author Tarita Ana
 */
@Entity
@Table(name = "CarImage")
public class CarImage {

    public static final String IMAGE_PREFIX_URL = "/assets/images/";
    @Id
    @Column(name = "c_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;

    private String imgHash;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getImgHash() {
        return imgHash;
    }

    public void setImgHash(String imgHash) {
        this.imgHash = imgHash;
    }

    @Transient
    public String getImageUrl() {
        return IMAGE_PREFIX_URL + getImgHash();
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "car");
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "car");
    }
}
