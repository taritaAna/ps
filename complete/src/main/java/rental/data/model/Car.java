package rental.data.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.hibernate.annotations.Fetch;
import rental.data.model.util.CarType;
import rental.data.model.util.FuelType;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.time.Instant;
import java.util.*;

import static org.hibernate.annotations.FetchMode.JOIN;

/**
 * @author Tarita Ana
 */
@Entity
@Table(name = "Car")
@XmlRootElement(name = "car")
public class Car {
    public static final String DEFAULT_IMG = "/img/car.jpg";

    @Id
    @Column(name = "car_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private String location;
    private String make;
    private String model;
    private double rentalPrice;
    private short year;
    private int mileage;
    private FuelType fuelType;
    private CarType carType;

    @OneToMany(mappedBy = "car", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(JOIN)
    private Set<Application> applications = new HashSet<>();

    @OneToMany(mappedBy = "car", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @Fetch(JOIN)
    private Set<Auction> auctions = new HashSet<>();

    @OneToMany(mappedBy = "car", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    private Set<CarImage> images = new HashSet<>();

    @XmlElement
    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    @XmlElement
    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @XmlElement
    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    @XmlElement
    public double getRentalPrice() {
        return rentalPrice;
    }

    public void setRentalPrice(double rentalPrice) {
        this.rentalPrice = rentalPrice;
    }

    @XmlElement
    public short getYear() {
        return year;
    }

    public void setYear(short year) {
        this.year = year;
    }

    @XmlElement
    public int getMileage() {
        return mileage;
    }

    public void setMileage(int mileage) {
        this.mileage = mileage;
    }

    @XmlElement
    public FuelType getFuelType() {
        return fuelType;
    }

    public void setFuelType(FuelType fuelType) {
        this.fuelType = fuelType;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Set<Application> getApplications() {
        return applications;
    }

    public void setApplications(Set<Application> applications) {
        this.applications = applications;
    }

    public CarType getCarType() {
        return carType;
    }

    public void setCarType(CarType carType) {
        this.carType = carType;
    }

    public Set<CarImage> getImages() {
        return images;
    }

    public void setImages(Set<CarImage> images) {
        this.images = images;
    }

    public Set<Auction> getAuctions() {
        return auctions;
    }

    public void setAuctions(Set<Auction> auctions) {
        this.auctions = auctions;
    }

    @Transient
    public Auction getActiveAuction() {
        List<Auction> list = new ArrayList<Auction>(getAuctions());
        Comparator<Auction> byEndDate = (e1, e2) -> Long.compare(
                e1.getEndDate().toInstant().toEpochMilli(), e2.getEndDate().toInstant().toEpochMilli());

        return list.stream()
                .sorted(byEndDate)
                .filter(auction -> Instant.now().isBefore(auction.getEndDate().toInstant()))
                .findFirst()
                .orElse(null);
    }

    @Transient
    public String getMainImageUrl() {
        return getImages().stream().findFirst().map(CarImage::getImageUrl).orElse(DEFAULT_IMG);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "applications", "images");
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "applications", "images", "auctions");
    }
}
