package rental.data.model.util;

/**
 * @author Tarita Ana
 */
public enum FuelType {
    GASOLINE, DIESEL, ELECTRIC
}
