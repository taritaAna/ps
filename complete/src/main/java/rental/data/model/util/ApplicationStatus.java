package rental.data.model.util;

/**
 * @author Tarita Ana
 */
public enum ApplicationStatus {
    PENDING, APPROVED, CANCELED
}
