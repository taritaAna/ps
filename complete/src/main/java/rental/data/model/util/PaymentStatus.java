package rental.data.model.util;

/**
 * @author Tarita Ana
 */
public enum PaymentStatus {
    PENDING, SUCCESS, FAILED
}
