package rental.data.model.util;

/**
 * @author Tarita Ana
 */
public enum Role {
    ADMIN, CUSTOMER
}
