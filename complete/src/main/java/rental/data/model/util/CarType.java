package rental.data.model.util;

/**
 * @author Tarita Ana
 */
public enum CarType {
    STANDARD, WISHLISTED, STAR
}
