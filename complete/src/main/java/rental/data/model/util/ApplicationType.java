package rental.data.model.util;

/**
 * @author Tarita Ana
 */
public enum ApplicationType {
    RENTAL, TEST_DRIVE
}
