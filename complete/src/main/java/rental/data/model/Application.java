package rental.data.model;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import rental.data.model.util.ApplicationStatus;
import rental.data.model.util.ApplicationType;
import rental.data.model.util.PaymentStatus;

import javax.persistence.*;
import java.sql.Date;

/**
 * @author Tarita Ana
 */
@Entity
@Table(name = "Application")
public class Application {
    @Id
    @Column(name = "app_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @ManyToOne
    @JoinColumn(name = "car_id")
    private Car car;
    private Date startDate;
    private Date endDate;
    private boolean isApproved;
    private ApplicationType type;
    private ApplicationStatus appStatus = ApplicationStatus.PENDING;
    private PaymentStatus paymentStatus = PaymentStatus.PENDING;
    public static final String FORMAT_HEADER = "%20s %20s %10s %10s %4s %6s %6s %6s %10s %10s %10s";
    public static final String FORMAT_CONTENT = "%20s %20s %10s %10s %d %6d %10s %3.2f %10s %10s %10s";

    @ManyToOne
    @JoinColumn(name = "user_id")
    private User user;

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public boolean isApproved() {
        return isApproved;
    }

    public void setIsApproved(boolean isApproved) {
        this.isApproved = isApproved;
        if (isApproved) {
            setAppStatus(ApplicationStatus.APPROVED);
        } else {
            setAppStatus(ApplicationStatus.CANCELED);
        }
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public ApplicationType getType() {
        return type;
    }

    public void setType(ApplicationType type) {
        this.type = type;
    }

    public ApplicationStatus getAppStatus() {
        return appStatus;
    }

    public void setAppStatus(ApplicationStatus appStatus) {
        this.appStatus = appStatus;
    }

    public Date getStartDate() {
        return startDate;
    }

    public void setStartDate(Date startDate) {
        this.startDate = startDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public PaymentStatus getPaymentStatus() {
        return paymentStatus;
    }

    public void setPaymentStatus(PaymentStatus paymentStatus) {
        this.paymentStatus = paymentStatus;
    }

    @Transient
    public boolean isRental() {
        return getType() == ApplicationType.RENTAL;
    }

    @Transient
    public boolean isTestDrive() {
        return getType() == ApplicationType.TEST_DRIVE;
    }

    @Transient
    public static String getFields() {
        return String.format(FORMAT_HEADER, "Start Date", "End Date", "Make", "Model", "Year", "Mileage", "Fuel", "Price", "App. Status", "Payment", "App. Type");
    }

    @Transient
    public String getFieldsContent() {
        return String.format(FORMAT_CONTENT, startDate, endDate, car.getMake(), car.getModel(), car.getYear(), car.getMileage(), car.getFuelType(), car.getRentalPrice(), appStatus, paymentStatus, type);
    }

    @Override
    public int hashCode() {
        return HashCodeBuilder.reflectionHashCode(this, "car", "user");
    }

    @Override
    public boolean equals(Object obj) {
        return EqualsBuilder.reflectionEquals(this, obj, "car", "user");
    }
}
