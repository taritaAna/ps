package rental.data.report;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tarita Ana
 */
@XmlRootElement(name = "report")
public class CarReport implements Report {
    private String date;
    private List<PerCarReport> cars = new ArrayList<>();
    private long total;

    @XmlElement
    @Override
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @XmlElement
    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    @XmlElement
    public List<PerCarReport> getCars() {
        return cars;
    }

    public void setCars(List<PerCarReport> cars) {
        this.cars = cars;
    }

    public static class PerCarReport {
        private int id;
        private String dateAdded;
        private String make;
        private String model;
        private long rentals;
        private long testDrives;
        private double rentalPrice;
        private double profit;

        public String getModel() {
            return model;
        }

        public void setModel(String model) {
            this.model = model;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getDateAdded() {
            return dateAdded;
        }

        public void setDateAdded(String dateAdded) {
            this.dateAdded = dateAdded;
        }

        public String getMake() {
            return make;
        }

        public void setMake(String make) {
            this.make = make;
        }

        public long getRentals() {
            return rentals;
        }

        public void setRentals(long rentals) {
            this.rentals = rentals;
        }

        public long getTestDrives() {
            return testDrives;
        }

        public void setTestDrives(long testDrives) {
            this.testDrives = testDrives;
        }

        public double getRentalPrice() {
            return rentalPrice;
        }

        public void setRentalPrice(double rentalPrice) {
            this.rentalPrice = rentalPrice;
        }

        public double getProfit() {
            return profit;
        }

        public void setProfit(double profit) {
            this.profit = profit;
        }
    }
}
