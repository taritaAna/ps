package rental.data.report;


import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Tarita Ana
 */
@XmlRootElement(name = "report")
public class UserReport implements Report {
    private String date;
    private List<PerUserReport> users = new ArrayList<>();

    private long total;

    @XmlElement
    @Override
    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @XmlElement
    public List<PerUserReport> getUsers() {
        return users;
    }

    public void setUsers(List<PerUserReport> users) {
        this.users = users;
    }

    @XmlElement
    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public static class PerUserReport {
        private String registrationDate;
        private String email;
        private long rentals;
        private long testDrives;

        public String getRegistrationDate() {
            return registrationDate;
        }

        public void setRegistrationDate(String registrationDate) {
            this.registrationDate = registrationDate;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public long getRentals() {
            return rentals;
        }

        public void setRentals(long rentals) {
            this.rentals = rentals;
        }

        public long getTestDrives() {
            return testDrives;
        }

        public void setTestDrives(long testDrives) {
            this.testDrives = testDrives;
        }
    }
}
