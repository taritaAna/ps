package rental.data.report;

/**
 * @author Tarita Ana
 */
public interface Report {
    String getDate();
}
