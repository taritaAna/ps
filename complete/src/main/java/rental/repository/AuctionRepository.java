package rental.repository;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import rental.data.model.Auction;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Repository
public class AuctionRepository {

    @Resource
    private SessionFactory sessionFactory;

    public void add(Auction auction) {
        sessionFactory.getCurrentSession().persist(auction);
    }

    public Auction fetch(int id) {
        Auction auction = (Auction) sessionFactory.getCurrentSession().get(Auction.class, id);
        return auction;
    }

    public Auction fetchByCar(int carId) {
        Date time = new Date();
        return (Auction) sessionFactory.getCurrentSession().createQuery("from Auction a where a.car.id= :carId and a.endDate > :time")
                .setParameter("carId", carId)
                .setParameter("time", time)
                .uniqueResult();
    }

    public List<Auction> fetchAuctions() {
        return sessionFactory.getCurrentSession().createQuery("from Auction").list();
    }

    public void update(Auction auction) {
        sessionFactory.getCurrentSession().merge(auction);
    }
}
