package rental.repository;

import org.hibernate.Criteria;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.stereotype.Repository;
import rental.data.model.Bid;

import javax.annotation.Resource;

/**
 * @author Tarita Ana
 */
@Repository
public class BidRepository {

    @Resource
    private SessionFactory sessionFactory;

    public double getHighestBidForAuction(int id) {
        Criteria criteria = sessionFactory.getCurrentSession()
                .createCriteria(Bid.class)
                .add(Restrictions.eq("auction_id", id))
                .setProjection(Projections.max("bidAmount"));

        return (Double) criteria.uniqueResult();
    }

    public void add(Bid bid) {
        sessionFactory.getCurrentSession().persist(bid);
    }
}
