package rental.repository;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import rental.data.model.util.ApplicationStatus;
import rental.data.model.Wishlist;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Repository
public class WishlistRepository {

    @Resource
    private SessionFactory sessionFactory;

    public List<Wishlist> fetchPendingWishes() {
        return sessionFactory.getCurrentSession().createQuery("from Wishlist w where w.appStatus =:appStatus")
                .setParameter("appStatus", ApplicationStatus.PENDING)
                .list();
    }

    public List<Wishlist> fetchWishlistByUser(String username) {
        return sessionFactory.getCurrentSession().createQuery("from Wishlist w where w.user.userName =:username")
                .setParameter("username", username)
                .list();
    }

    public void add(Wishlist wish) {
        sessionFactory.getCurrentSession().persist(wish);
    }

    public Wishlist fetch(int id) {
        Wishlist wish = (Wishlist) sessionFactory.getCurrentSession().get(Wishlist.class, id);
        return wish;
    }

    public void delete(Wishlist wish) {
        sessionFactory.getCurrentSession().delete(wish);
    }

    public void update(Wishlist wish) {
        sessionFactory.getCurrentSession().merge(wish);
    }
}
