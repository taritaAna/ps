package rental.repository;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import rental.data.model.Application;
import rental.data.model.CarImage;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Repository
public class CarImageRepository {

    @Resource
    private SessionFactory sessionFactory;

    public void add(CarImage carImage) {
        sessionFactory.getCurrentSession().persist(carImage);
    }

    public List<CarImage> fetchImagesByCar(int id) {
        return sessionFactory.getCurrentSession().createQuery("from CarImage img  where img.car.id =:id")
                .setParameter("id", id)
                .list();
    }
}
