package rental.repository;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rental.data.model.Car;
import rental.data.model.util.CarType;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Repository
public class CarRepository {

    @Resource
    private SessionFactory sessionFactory;

    public List<Car> fetchCars(CarType carType) {
        return sessionFactory.getCurrentSession().createQuery("from Car c where c.carType = :carType")
                .setParameter("carType", carType)
                .list();
    }

    public Car fetch(final int id) {
        Car car = (Car) sessionFactory.getCurrentSession().get(Car.class, id);
        return car;
    }

    public void update(Car car) {
        sessionFactory.getCurrentSession().merge(car);
    }

    public void add(Car car) {
        sessionFactory.getCurrentSession().persist(car);
    }

    public void delete(Car car) {
        sessionFactory.getCurrentSession().delete(car);
    }

    public List<Car> searchCars(final String query) {
        return sessionFactory.getCurrentSession().createQuery("from Car c where c.carType = :carType and c.model like :query or c.make like :query or c.location like :query")
                .setParameter("carType", CarType.STANDARD)
                .setParameter("query", "%" + query + "%").list();
    }


}
