package rental.repository;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import rental.data.model.User;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Repository
public class UserRepository {

    @Resource
    private SessionFactory sessionFactory;

    public List<User> fetchUsers() {
        return sessionFactory.getCurrentSession().createQuery("from User").list();
    }

    public User fetch(final int id) {
        return (User) sessionFactory.getCurrentSession().get(User.class, id);
    }

    public void update(User user) {
        sessionFactory.getCurrentSession().update(user);
    }

    public void add(User user) {
        sessionFactory.getCurrentSession().persist(user);
    }

    public void delete(int carId) {
        User user = new User();
        user.setId(carId);
        sessionFactory.getCurrentSession().delete(user);
    }

    public User fetchByUsername(String username) {
        User user = null;
        Object o = sessionFactory.getCurrentSession().createQuery("from User where userName=:userName").setParameter("userName", username).uniqueResult();
        if (o != null) {
            user = (User) o;
        }
        return user;
    }

    public User fetchByUsernameOrEmail(String usernameOrEmail) {
        User user = null;
        Object o = sessionFactory.getCurrentSession().createQuery("from User where userName=:val or mail=:val")
                .setParameter("val", usernameOrEmail)
                .uniqueResult();

        if (o != null) {
            user = (User) o;
        }
        return user;
    }

    public List<User> fetchByNotification() {
        return sessionFactory.getCurrentSession()
                .createQuery("from User where notifyStarCars=:notifyStarCars")
                .setParameter("notifyStarCars", true)
                .list();
    }
}
