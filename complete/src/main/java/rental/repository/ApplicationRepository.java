package rental.repository;

import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import rental.data.model.Application;
import rental.data.model.util.ApplicationType;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Repository
public class ApplicationRepository {

    @Resource
    private SessionFactory sessionFactory;

    public List<Application> fetchStandardApplications() {
        return sessionFactory.getCurrentSession().createQuery("from Application").list();
    }

    public List<Application> fetchApplicationsByUser(String username) {
        return sessionFactory.getCurrentSession().createQuery("from Application a where a.user.userName =:username")
                .setParameter("username", username)
                .list();
    }

    public Application fetch(final int id) {
        return (Application) sessionFactory.getCurrentSession().get(Application.class, id);
    }

    public void update(Application application) {
        sessionFactory.getCurrentSession().update(application);
    }

    public void add(Application application) {
        sessionFactory.getCurrentSession().persist(application);
    }

    public void delete(int appId) {
        Application application = new Application();
        application.setId(appId);
        sessionFactory.getCurrentSession().delete(application);
    }

    public List<Application> fetchApprovedApplicationsForCar(ApplicationType type, int id, Date startDate, Date endDate) {
        boolean isApproved = true;
        return sessionFactory.getCurrentSession().createQuery("from Application a where a.car.id =:id and a.type = :type " +
                "and a.startDate = :startDate and a.endDate = :endDate and a.isApproved =:isApproved")
                .setParameter("id", id)
                .setParameter("type", type)
                .setParameter("startDate", startDate)
                .setParameter("endDate", endDate)
                .setParameter("isApproved", isApproved)
                .list();
    }

}
