package rental.controller;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.web.bind.annotation.ModelAttribute;

import java.security.Principal;

/**
 * @author Tarita Ana
 */
public class BaseController {
    @ModelAttribute("username")
    public String username(Principal principal) {
        return principal.getName();
    }

    @ModelAttribute("userRole")
    public String userRole(Principal principal) {
        return ((Authentication) principal).getAuthorities().stream()
                .map(GrantedAuthority::getAuthority)
                .findFirst()
                .get();
    }
}
