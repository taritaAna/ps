package rental.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rental.data.model.Car;
import rental.data.model.Wishlist;
import rental.service.ApplicationService;
import rental.service.CarService;
import rental.service.WishlistService;

import javax.annotation.Resource;
import java.security.Principal;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping("/wishlist")
public class WishlistController extends BaseController{

    @Resource
    private WishlistService wishlistService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewAllApplications(ModelMap modelMap, Principal principal) {
        String username = principal.getName();
        modelMap.addAttribute("wishlist", wishlistService.fetchWishlistByUser(username));
        return "wishlist";
    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.GET)
    public String makeAWish(ModelMap modelMap) {
        modelMap.addAttribute("car", new Car());
        return "wish";
    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.POST)
    public String makeAWish(@ModelAttribute Car car, ModelMap modelMap, Principal principal) {
        wishlistService.add(car, principal.getName());

        return "redirect:/wishlist";
    }

    @RequestMapping(value = {"/{id}/delete"}, method = RequestMethod.POST)
    public String deleteWish(@PathVariable("id") int id) {
        wishlistService.delete(id);
        return "redirect:/wishlist";
    }
}

