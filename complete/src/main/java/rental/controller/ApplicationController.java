package rental.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rental.data.model.Application;
import rental.service.ApplicationService;
import rental.service.email.EmailService;
import rental.service.PaymentService;
import rental.service.UserService;

import javax.annotation.Resource;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.security.Principal;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping(value = {"/applications"})
public class ApplicationController extends BaseController {

    @Resource
    private ApplicationService applicationService;

    @Resource
    private PaymentService paymentService;

    @Resource
    private UserService userService;

    @Resource
    private EmailService emailService;

    public static final String GENERATED_REPORTS = "Generated reports";
    public static final String SUBJECT_REPORTS = "Applications' report";

    @RequestMapping(method = RequestMethod.GET)
    public String viewAllApplications(ModelMap modelMap, Principal principal) {
        String username = principal.getName();
        modelMap.addAttribute("applications", applicationService.fetchApplicationsByUser(username));
        return "applications";
    }

    @RequestMapping(value = {"/{id}/pay"}, method = RequestMethod.POST)
    public String updateApplication(@PathVariable("id") int appId, Principal principal) {
        Application application = applicationService.fetch(appId);
        paymentService.processPayment(application);
        return "redirect:/applications";
    }

    @RequestMapping(value = {"/reports"}, method = RequestMethod.GET, produces = "text/plain")
    public void report(HttpServletResponse response, Principal principal) throws IOException {
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=applications.txt");
        String username = principal.getName();
        ServletOutputStream output = response.getOutputStream();

        List<Application> applications = applicationService.fetchApplicationsByUser(username);
        output.println(Application.getFields());
        for (Application application : applications) {
            output.println(application.getFieldsContent());
        }
    }

    @RequestMapping(value = {"/reports/send"}, method = RequestMethod.GET)
    public String reportByMail(Principal principal) throws IOException {
        String username = principal.getName();
        List<Application> applications = applicationService.fetchApplicationsByUser(username);
        String email = userService.fetchByUsername(username).getMail();
        emailService.prepareAndSendUserReportEmail(email, SUBJECT_REPORTS, GENERATED_REPORTS, applications);
        return "redirect:/applications";
    }
}
