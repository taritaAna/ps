package rental.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @author Tarita Ana
 */
@Controller
public class ErrorController {

    @RequestMapping("/500")
     public String internalError() {
        return "500";
    }

    @RequestMapping("/404")
    public String notFoundError() {
        return "404";
    }
}
