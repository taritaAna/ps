package rental.controller;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * @author Tarita Ana
 */
@Controller
public class AssetsController {

    @Value("${img.storage.location}")
    private String imgLocation;

    @RequestMapping(value = "/assets/images/{hash}", method = RequestMethod.GET)
     public void serveImage(@PathVariable("hash") String hash, HttpServletResponse response) throws IOException {
        File image = new File(imgLocation + hash);
        try (FileInputStream stream = new FileInputStream(image)) {
            IOUtils.copy(stream, response.getOutputStream());
        }
    }

}
