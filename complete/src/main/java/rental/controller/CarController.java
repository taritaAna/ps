package rental.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import rental.data.model.Application;
import rental.data.model.Bid;
import rental.data.model.util.ApplicationType;
import rental.data.model.util.CarType;
import rental.service.ApplicationService;
import rental.service.AuctionService;
import rental.service.CarService;
import rental.service.UserService;

import javax.annotation.Resource;
import java.security.Principal;
import java.sql.Date;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping(value = {"/cars"})
public class CarController extends BaseController {

    @Resource
    private CarService carService;

    @Resource
    private UserService userService;

    @Resource
    private ApplicationService applicationService;

    @Resource
    private AuctionService auctionService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewAllCars(ModelMap modelMap, Principal principal, @RequestParam(name = "type", defaultValue = "STANDARD") CarType carType) {
        modelMap.addAttribute("cars", carService.fetchByType(carType));
        modelMap.addAttribute("carTypes", carType);
        modelMap.addAttribute("user", userService.fetchByUsername(principal.getName()));
        return "cars";
    }

    @RequestMapping(value = {"/{id}/rent"}, method = RequestMethod.GET)
    public String addRental(ModelMap modelMap) {
        modelMap.addAttribute("type", ApplicationType.RENTAL);
        modelMap.addAttribute("carApplication", new Application());
        return "application";
    }

    @RequestMapping(value = {"/{id}/rent"}, method = RequestMethod.POST)
    public String addRental(@PathVariable("id") int carId, @RequestParam Date startDate, Date endDate, Principal principal) {
        String username = principal.getName();
        applicationService.add(ApplicationType.RENTAL, carId, username, startDate, endDate);
        return "redirect:/applications";
    }

    @RequestMapping(value = {"/{id}/test"}, method = RequestMethod.GET)
    public String addTestDrive(ModelMap modelMap) {
        modelMap.addAttribute("type", ApplicationType.TEST_DRIVE);
        modelMap.addAttribute("carApplication", new Application());
        return "application";
    }

    @RequestMapping(value = {"/{id}/test"}, method = RequestMethod.POST)
    public String addTestDrive(@PathVariable("id") int carId, @RequestParam Date date, Principal principal) {
        String username = principal.getName();
        applicationService.add(ApplicationType.TEST_DRIVE, carId, username, date, date);
        return "redirect:/applications";
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String searchCars(@RequestParam("query") String query, ModelMap modelMap) {
        modelMap.addAttribute("cars", carService.searchCars(query));
        return "cars";
    }

    @RequestMapping(value = {"/{id}/view"}, method = RequestMethod.GET)
    public String viewCar(@PathVariable("id") int carId, ModelMap modelMap) {
        modelMap.addAttribute("car", carService.fetch(carId));
        return "car";
    }

    @RequestMapping(value = {"/{id}/viewAndBid"}, method = RequestMethod.GET)
    public String viewCarAndBid(@PathVariable("id") int carId, ModelMap modelMap) {
        modelMap.addAttribute("car", carService.fetch(carId));
        modelMap.addAttribute("bid", new Bid());
        return "bid";
    }

    @RequestMapping(value = {"/{id}/viewAndBid"}, method = RequestMethod.POST)
    public String placeBid(@PathVariable("id") int carId, @ModelAttribute Bid bid, Principal principal) {
        auctionService.placeBid(bid.getBidAmount(), carService.fetch(carId).getActiveAuction().getId(), principal.getName());
        return "redirect:/cars?type=STAR";
    }
}
