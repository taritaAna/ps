package rental.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import rental.data.model.User;
import rental.service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping("/profile")
public class ProfileController extends BaseController {

    @Resource
    private UserService userService;

    @RequestMapping(value = "/notification", method = RequestMethod.POST)
     public void setNotificationPreferences(HttpServletResponse response, @RequestParam boolean notifyStarCars, Principal principal) {
        User currentUser = userService.fetchByUsername(principal.getName());
        currentUser.setNotifyStarCars(notifyStarCars);
        userService.update(currentUser);
        response.setStatus(202);
        return;
    }

}
