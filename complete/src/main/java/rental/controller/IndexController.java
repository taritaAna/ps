package rental.controller;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rental.service.CarService;

import javax.annotation.Resource;
import java.security.Principal;

/**
 * @author Tarita Ana
 */
@Controller
public class IndexController extends BaseController {

    @Resource
    private CarService carService;

    @RequestMapping(value = {"/"}, method = RequestMethod.GET)
    public String index(ModelMap modelMap, Principal principal) {
        return "index";
    }


}
