package rental.controller;

import org.owasp.esapi.errors.EncryptionException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import rental.data.model.User;
import rental.service.email.EmailService;
import rental.service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.net.URLEncoder;

/**
 * @author Tarita Ana
 */
@Controller
public class ForgotPasswordController {

    @Resource
    private UserService userService;

    @Resource
    private EmailService emailService;

    @RequestMapping(value = {"/forgot-password"}, method = RequestMethod.GET)
    public String forgotPasswordPage() {
        return "forgot-password";
    }

    @RequestMapping(value = {"/forgot-password-sent"}, method = RequestMethod.GET)
    public String forgotPasswordSentPage() {
        return "forgot-password-sent";
    }

    @RequestMapping(value = {"/forgot-password"}, method = RequestMethod.POST)
    public String forgotPassword(HttpServletRequest request, @RequestParam("usernameOrMail") String usernameOrMail) throws EncryptionException, IOException {

        User user = userService.fetchByUsernameOrEmail(usernameOrMail);
        if (user == null) {
            return "redirect:/forgot-password?error=invalid-user";
        }
        emailService.prepareAndSendForgotPassEmail(user, request.getServerName(), request.getServerPort());
        return "redirect:/forgot-password-sent";
    }

    @RequestMapping(value = {"/reset-password"}, method = RequestMethod.GET)
    public String passwordResetForm(ModelMap modelMap, @RequestParam("key") String key) throws EncryptionException {
        User user = userService.getUserByPasswordResetKey(key);
        if (user == null) {
            throw new UsernameNotFoundException("How did this happen?");
        }
        modelMap.put("name", user.getFirstName() + " " + user.getLastName());
        modelMap.put("username", user.getUserName());
        return "password-reset";
    }

    @RequestMapping(value = {"/reset-password"}, method = RequestMethod.POST)
    public String passwordResetPost(@RequestParam("key") String key, @RequestParam String password,
                                    @RequestParam String passwordConfirm) throws EncryptionException {
        User user = userService.getUserByPasswordResetKey(key);
        if (!password.equals(passwordConfirm)) {
            return "redirect:/reset-password?key=" + URLEncoder.encode(key) + "&error=password-mismatch";
        }
        if (user == null) {
            throw new UsernameNotFoundException("How did this happen?");
        }
        user.setPassword(password);
        userService.hashPasswordAndUpdate(user);
        return "redirect:/";
    }
}
