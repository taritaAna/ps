package rental.controller.admin;

import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import rental.controller.BaseController;
import rental.data.model.Auction;
import rental.data.model.Car;
import rental.data.model.util.CarType;
import rental.data.report.Report;
import rental.service.AuctionService;
import rental.service.CarService;
import rental.service.report.AdminCarReportGenerator;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping(value = {"/admin/cars"})
public class AdminCarController extends BaseController {

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssX");
        dateFormat.setLenient(true);
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @Resource
    private CarService carService;

    @Resource
    private AdminCarReportGenerator carReportGenerator;

    @Resource
    private AuctionService auctionService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewAllCars(ModelMap modelMap, @RequestParam(name = "type", defaultValue = "STANDARD") CarType carType) {
        modelMap.addAttribute("cars", carService.fetchByType(carType));
        return "admin/cars";
    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.GET)
    public String createCar(ModelMap modelMap) {
        modelMap.addAttribute("car", new Car());
        return "admin/car";
    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.POST)
    public String createCar(@ModelAttribute Car car, ModelMap modelMap) {
        carService.add(car);

        return "redirect:/admin/cars";
    }

    @RequestMapping(value = {"/upload"}, method = RequestMethod.POST)
    public String upload(MultipartFile file) throws JAXBException, IOException {
        JAXBContext jaxbContext = JAXBContext.newInstance(Car.class);
        Unmarshaller jaxbMarshaller = jaxbContext.createUnmarshaller();
        Car car = (Car) jaxbMarshaller.unmarshal(file.getInputStream());
        carService.add(car);
        return "redirect:/admin/cars";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.GET)
    public String edit(@PathVariable("id") int carId, ModelMap modelMap) {
        modelMap.addAttribute("car", carService.fetch(carId));
        return "admin/car";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
    public String updateCar(@PathVariable("id") int carId, @ModelAttribute Car car, ModelMap modelMap) throws IOException {
        carService.update(car);
        return "redirect:/admin/cars";
    }

    @RequestMapping(value = {"/{id}/delete"}, method = RequestMethod.POST)
    public String updateCar(@PathVariable("id") int carId) {
        carService.delete(carId);
        return "redirect:/admin/cars";
    }

    @RequestMapping(value = {"/reports.xml"}, method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public Report xmlReports(HttpServletResponse response) {
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=reports.xml");
        return carReportGenerator.createReport();
    }

    @RequestMapping(value = {"/reports.json"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Report jsonReports(HttpServletResponse response) {
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=reports.json");
        return carReportGenerator.createReport();
    }

    @RequestMapping(value = {"/{id}/upload"}, method = RequestMethod.POST)
    public String uploadImage(MultipartFile file, @PathVariable("id") int carId) throws JAXBException, IOException {
        carService.addImage(carId, file);
        return "redirect:/admin/cars";
    }

    @RequestMapping(value = {"/{id}/auction"}, method = RequestMethod.GET)
    public String auction(@PathVariable("id") int carId, ModelMap modelMap) {
        modelMap.addAttribute("car", carService.fetch(carId));
        modelMap.addAttribute("auction", new Auction());
        return "/admin/auction";
    }

    @RequestMapping(value = {"/{carId}/auction"}, method = RequestMethod.POST)
    public String auction(@PathVariable("carId") int carId, @ModelAttribute Auction auction, ModelMap modelMap) {
        auction.setCar(carService.fetch(carId));
        auctionService.add(auction);
        return "redirect:/admin/cars?type=STAR";
    }

    @RequestMapping(value = {"/{id}/view"}, method = RequestMethod.GET)
    public String viewCar(@PathVariable("id") int carId, ModelMap modelMap) {
        modelMap.addAttribute("car", carService.fetch(carId));
        return "/admin/carView";
    }
}

