package rental.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rental.controller.BaseController;
import rental.data.model.Application;
import rental.service.ApplicationService;

import javax.annotation.Resource;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping(value = {"/admin/applications"})
public class AdminApplicationController extends BaseController {

    @Resource
    private ApplicationService applicationService;

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("applications", applicationService.fetchStandardApplications());
        return "admin/applications";
    }

    @RequestMapping(value = {"/{id}/cancel"}, method = RequestMethod.POST)
    public String cancel(@PathVariable("id") int appId) {
        Application application = applicationService.canCancel(appId);
        applicationService.update(application);
        return "redirect:/admin/applications";
    }

    @RequestMapping(value = {"/{id}/approve"}, method = RequestMethod.POST)
    public String approve(@PathVariable("id") int appId) {
        Application application = applicationService.canApprove(appId);
        applicationService.update(application);
        return "redirect:/admin/applications";
    }

    @RequestMapping(value = {"/{id}/delete"}, method = RequestMethod.POST)
    public String updateApplication(@PathVariable("id") int appId) {
        applicationService.delete(appId);
        return "redirect:/admin/applications";
    }
}
