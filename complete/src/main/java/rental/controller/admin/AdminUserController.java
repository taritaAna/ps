package rental.controller.admin;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import rental.controller.BaseController;
import rental.data.model.User;
import rental.data.report.Report;
import rental.service.report.AdminUserReportGenerator;
import rental.service.UserService;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping(value = {"/admin/users"})
public class AdminUserController extends BaseController {

    @Resource
    private UserService userService;

    @Resource
    private AdminUserReportGenerator userReportGenerator;

    @RequestMapping(method = RequestMethod.GET)
    public String index(ModelMap modelMap) {
        modelMap.addAttribute("users", userService.fetchUsers());
        return "admin/users";
    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.GET)
    public String createUser(ModelMap modelMap) {
        modelMap.addAttribute("user", new User());
        return "admin/user";
    }

    @RequestMapping(value = {"/add"}, method = RequestMethod.POST)
    public String createUser(@ModelAttribute User user, ModelMap modelMap) {
        userService.add(user);
        return "redirect:/admin/users";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.GET)
    public String edit(@PathVariable("id") int userId, ModelMap modelMap) {
        modelMap.addAttribute("user", userService.fetch(userId));
        return "admin/user";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
    public String updateUser(@PathVariable("id") int userId, @ModelAttribute User user, ModelMap modelMap) {
        userService.hashPasswordAndUpdate(user);
        return "redirect:/admin/users";
    }

    @RequestMapping(value = {"/{id}/delete"}, method = RequestMethod.POST)
    public String updateUser(@PathVariable("id") int userId) {
        userService.delete(userId);
        return "redirect:/admin/users";
    }

    @RequestMapping(value = {"/reports.xml"}, method = RequestMethod.GET, produces = "application/xml")
    @ResponseBody
    public Report xmlReports(HttpServletResponse response) {
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=reports.xml");
        return userReportGenerator.createReport();
    }

    @RequestMapping(value = {"/reports.json"}, method = RequestMethod.GET, produces = "application/json")
    @ResponseBody
    public Report jsonReports(HttpServletResponse response) {
        response.addHeader(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=reports.json");
        return userReportGenerator.createReport();
    }
}
