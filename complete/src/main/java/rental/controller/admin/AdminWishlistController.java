package rental.controller.admin;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import rental.controller.BaseController;
import rental.data.model.Car;
import rental.service.WishlistService;

import javax.annotation.Resource;
import java.io.IOException;
import java.security.Principal;

/**
 * @author Tarita Ana
 */
@Controller
@RequestMapping("/admin/wishlist")
public class AdminWishlistController extends BaseController{

    @Resource
    private WishlistService wishlistService;

    @RequestMapping(method = RequestMethod.GET)
    public String viewAllApplications(ModelMap modelMap) {
        modelMap.addAttribute("wishlist", wishlistService.fetchWishlist());
        return "/admin/wishlist";
    }

    @RequestMapping(value = {"/{id}/delete"}, method = RequestMethod.POST)
    public String deleteWish(@PathVariable("id") int id) {
        wishlistService.cancel(id);
        return "redirect:/wishlist";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.GET)
    public String edit(@PathVariable("id") int wishId, ModelMap modelMap) {
        modelMap.addAttribute("car", wishlistService.fetch(wishId).getCar());
        return "admin/wish";
    }

    @RequestMapping(value = {"/{id}/edit"}, method = RequestMethod.POST)
    public String updateWish(@PathVariable("id") int wishId, @ModelAttribute Car car, ModelMap modelMap) throws IOException {
        wishlistService.approve(wishId, car);
        return "redirect:/admin/wishlist";
    }
}

