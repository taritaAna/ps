package rental.service;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rental.data.model.Auction;
import rental.data.model.Bid;
import rental.data.model.User;
import rental.event.AuctionAddedEvent;
import rental.repository.AuctionRepository;
import rental.repository.BidRepository;
import rental.repository.UserRepository;

import javax.annotation.Resource;
import java.time.Instant;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Service
@Transactional
public class AuctionService {

    @Resource
    private AuctionRepository auctionRepository;

    @Resource
    private BidRepository bidRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private ApplicationContext applicationContext;

    public void add(Auction auction) {
        auctionRepository.add(auction);
        applicationContext.publishEvent(new AuctionAddedEvent(auction));
    }

    public void placeBid(double amount, int auctionId, String username) {
        Auction auction = auctionRepository.fetch(auctionId);
        if (auction.getEndDate().toInstant().isAfter(Instant.now())) {
            double highest = auction.getHighestBid();
            User user = userRepository.fetchByUsername(username);
            if (amount > highest) {
                Bid bid = new Bid();
                bid.setAuction(auction);
                bid.setBidAmount(amount);
                bid.setUser(user);
                bidRepository.add(bid);
                return;
            }
        }
        //TODO: handle else and show allert
    }

    public Auction fetch(int auctionId) {
        return auctionRepository.fetch(auctionId);
    }

    public void update(Auction auction) {
        auctionRepository.update(auction);
    }

    public List<Auction> fetchAuctions() {
        return auctionRepository.fetchAuctions();
    }

}
