package rental.service;

import com.twocheckout.Twocheckout;
import com.twocheckout.TwocheckoutCharge;
import com.twocheckout.model.Authorization;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rental.data.model.Application;
import rental.data.model.util.PaymentStatus;
import rental.data.model.User;

import javax.annotation.Resource;
import java.time.Period;
import java.util.HashMap;

/**
 * @author Tarita Ana
 */
@Service
@Transactional
public class PaymentService {

    @Resource
    private ApplicationService applicationService;
    /**
     * Must - have setting for testing 2checkout integration in sandbox mode.
     */
    private static final String MODE = "sandbox";
    /**
     * 2checkout account identification as a seller.
     */
    private static final int SELLER_ID = 901314378;
    /**
     * Should be regenerated for each transaction by using seller_id and a new publishable_key.
     * http://jsbin.com/biguyojala/edit?html,output
     */
    private static final String TOKEN_ID = "OTI4ZjM4MjUtMDdjZC00ZGYxLWE4NzMtNzNkNzhjYTMxMmM3";
    /**
     * The key is valid for 60 minutes and 1 transaction.
     */
    private static final String PRIVATE_KEY = "32B9F17A-C725-4270-BF2B-B7EE22A457D9";

    public void processPayment(Application application) {
        Twocheckout.privatekey = PRIVATE_KEY;
        Twocheckout.mode = MODE;
        User user = application.getUser();
        try {
            HashMap<String, String> billing = new HashMap<String, String>();
            billing.put("name", user.getLastName()+" "+ user.getFirstName());
            billing.put("addrLine1", "Cluj-Napoca");
            billing.put("city", "Columbus");
            billing.put("state", "Ohio");
            billing.put("country", "USA");
            billing.put("zipCode", "43230");
            billing.put("email", "tester@2co.com");
            billing.put("phone", "555-555-5555");

            HashMap<String, Object> request = new HashMap<String, Object>();
            request.put("sellerId", SELLER_ID);
            request.put("merchantOrderId", application.getType());
            request.put("token", TOKEN_ID);
            request.put("currency", "EURO");
            request.put("total", computeAmount(application));
            request.put("billingAddr", billing);

            Authorization response = TwocheckoutCharge.authorize(request);
            application.setPaymentStatus(PaymentStatus.SUCCESS);
            System.out.println(response.getResponseMsg());
        } catch (Exception e) {
            e.printStackTrace();
            application.setPaymentStatus(PaymentStatus.FAILED);
        }
        finally {
            applicationService.update(application);
        }
    }

    private double computeAmount(Application application) {
        Period period = Period.between(application.getStartDate().toLocalDate(), application.getEndDate().toLocalDate());
        return period.getDays() * application.getCar().getRentalPrice();
    }

}
