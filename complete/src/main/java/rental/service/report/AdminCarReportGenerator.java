package rental.service.report;

import org.springframework.stereotype.Service;
import rental.data.model.Application;
import rental.data.model.util.CarType;
import rental.data.report.CarReport;
import rental.data.report.Report;
import rental.service.CarService;

import javax.annotation.Resource;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.Set;

import static java.util.stream.Collectors.toList;

/**
 * @author Tarita Ana
 */
@Service
public class AdminCarReportGenerator implements ReportGenerator {

    @Resource
    private CarService carService;

    @Override
    public Report createReport() {
        CarReport report = new CarReport();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;
        report.setDate(dateTimeFormatter.format(Instant.now()));

        report.setCars(carService.fetchByType(CarType.STANDARD).stream().map(car -> {
            CarReport.PerCarReport details = new CarReport.PerCarReport();
            details.setId(car.getId());
            details.setMake(car.getMake());
            details.setModel(car.getModel());
            details.setRentalPrice(car.getRentalPrice());
            Set<Application> applications = car.getApplications();
            details.setRentals(applications.stream().filter(Application::isRental).count());
            details.setTestDrives(applications.size() - details.getRentals());
            details.setProfit(details.getRentals() * details.getRentalPrice());
            return details;
        }).collect(toList()));
        report.setTotal(report.getCars().size());

        return report;
    }
}
