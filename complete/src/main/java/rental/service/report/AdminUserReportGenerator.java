package rental.service.report;

import org.springframework.stereotype.Service;
import rental.data.model.Application;
import rental.data.report.Report;
import rental.data.report.UserReport;
import rental.service.UserService;

import javax.annotation.Resource;
import java.time.Instant;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toList;

/**
 * @author Tarita Ana
 */
@Service
public class AdminUserReportGenerator implements ReportGenerator {

    @Resource
    private UserService userService;

    @Override
    public Report createReport() {

        UserReport report = new UserReport();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ISO_INSTANT;
        report.setDate(dateTimeFormatter.format(Instant.now()));

        report.setUsers(userService.fetchUsers().stream().map(user -> {
            UserReport.PerUserReport details = new UserReport.PerUserReport();
            details.setEmail(user.getMail());
            Set<Application> applications = user.getApplications();
            details.setRentals(applications.stream().filter(Application::isRental).count());
            details.setTestDrives(applications.size() - details.getRentals());
            return details;
        }).collect(toList()));
        report.setTotal(report.getUsers().size());

        return report;
    }
}
