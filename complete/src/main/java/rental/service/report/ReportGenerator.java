package rental.service.report;

import rental.data.report.Report;

/**
 * @author Tarita Ana
 */
public interface ReportGenerator {
    Report createReport();
}
