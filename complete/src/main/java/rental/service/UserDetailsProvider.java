package rental.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import rental.data.model.User;

import java.util.Arrays;

/**
 * @author Tarita Ana
 */
@Service
public class UserDetailsProvider implements UserDetailsService {

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userService.fetchByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException("Could not locate user with username: " + username);
        }
        org.springframework.security.core.userdetails.User springUser =
                new org.springframework.security.core.userdetails.User(user.getUserName(), user.getPassword(),
                        Arrays.asList(new SimpleGrantedAuthority(user.getRole().name())));
        return springUser;
    }
}
