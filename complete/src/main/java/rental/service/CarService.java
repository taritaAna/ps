package rental.service;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;
import rental.data.model.Application;
import rental.data.model.Car;
import rental.data.model.CarImage;
import rental.data.model.util.CarType;
import rental.repository.CarImageRepository;
import rental.repository.CarRepository;
import rental.service.email.EmailService;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Tarita Ana
 */
@Service
@Transactional
public class CarService {

    @Resource
    private CarRepository carRepository;

    @Resource
    private EmailService emailService;

    @Resource
    private CarImageRepository carImageRepository;

    @Value("${img.storage.location}")
    private String imgLocation;

    public List<Car> fetchByType(CarType type) {
        List<Car> cars = carRepository.fetchCars(type);
        if (type.equals(CarType.STAR))
            return cars.stream()
                    .filter(car -> car.getActiveAuction() != null)
                    .collect(Collectors.toList());
        return cars;
    }

    public Car fetch(final int id) {
        return carRepository.fetch(id);
    }

    public void update(Car car) throws IOException {
        Car oldCar = fetch(car.getId());
        if (oldCar.getRentalPrice() != car.getRentalPrice()) {
            notify(car, oldCar);
        }
        carRepository.update(car);
    }

    private void notify(Car car, Car oldCar) throws IOException {
        Set<Application> applications = oldCar.getApplications();
        List<String> emails = applications.stream()
                .map(application -> application.getUser().getMail())
                .distinct()
                .collect(Collectors.toList());
        emailService.prepareNotificationEmail(car, emails);
    }

    public void add(Car car) {
        carRepository.add(car);
    }

    public void delete(int carId) {
        Car car = new Car();
        car.setId(carId);
        carRepository.delete(car);
    }

    public List<Car> searchCars(final String query) {
        return carRepository.searchCars(query);
    }

    public void addImage(int carId, MultipartFile file) throws IOException {
        Car car = fetch(carId);
        File tmpUploadFile = File.createTempFile("car-dealer", ".img");
        FileOutputStream tmpFileStream = new FileOutputStream(tmpUploadFile);
        IOUtils.copy(file.getInputStream(), tmpFileStream);
        IOUtils.closeQuietly(file.getInputStream());
        IOUtils.closeQuietly(tmpFileStream);
        String hash;

        try (FileInputStream tmpInput = new FileInputStream(tmpUploadFile)) {
            hash = DigestUtils.sha256Hex(tmpInput);
        }
        String filePath = imgLocation + hash;
        FileUtils.moveFile(tmpUploadFile, new File(filePath));

        CarImage carImage = new CarImage();
        carImage.setCar(car);
        carImage.setImgHash(hash);
        carImageRepository.add(carImage);
    }

    public List<CarImage> fetchImages(int carId) {
        return carImageRepository.fetchImagesByCar(carId);
    }
}
