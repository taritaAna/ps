package rental.service;

import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rental.data.model.*;
import rental.data.model.util.ApplicationStatus;
import rental.data.model.util.CarType;
import rental.event.WishGrantedEvent;
import rental.repository.CarRepository;
import rental.repository.UserRepository;
import rental.repository.WishlistRepository;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Service
@Transactional
public class WishlistService {

    @Resource
    private WishlistRepository wishlistRepository;

    @Resource
    private CarRepository carRepository;

    @Resource
    private UserRepository userRepository;

    @Resource
    private ApplicationContext applicationContext;

    public List<Wishlist> fetchWishlist() {
        return wishlistRepository.fetchPendingWishes();
    }


    public List<Wishlist> fetchWishlistByUser(String username) {
        return wishlistRepository.fetchWishlistByUser(username);
    }


    public void add(Car car, String username) {
        car.setCarType(CarType.WISHLISTED);
        carRepository.add(car);
        User user = userRepository.fetchByUsername(username);

        Wishlist wish = new Wishlist();
        wish.setCar(car);
        wish.setUser(user);
        wishlistRepository.add(wish);
    }

    public void delete(int id) {
        Wishlist wish = wishlistRepository.fetch(id);
        Car car = wish.getCar();
        User user = wish.getUser();
        user.getWishlist().remove(wish);

        wish.setUser(null);
        wish.setCar(null);
        wishlistRepository.delete(wish);
        carRepository.delete(car);
    }

    public void cancel(int id) {
        Wishlist wish = wishlistRepository.fetch(id);
        wish.setAppStatus(ApplicationStatus.CANCELED);
        wishlistRepository.update(wish);
    }

    public Wishlist fetch(int wishId) {
        return wishlistRepository.fetch(wishId);
    }

    public void approve(int wishId, Car car) {
        Wishlist wish = wishlistRepository.fetch(wishId);
        Car wishCar = wish.getCar();
        wishCar.setRentalPrice(car.getRentalPrice());
        wishCar.setCarType(CarType.STANDARD);
        wishCar.setMileage(car.getMileage());
        wishCar.setFuelType(car.getFuelType());
        wishCar.setLocation(car.getLocation());
        carRepository.update(wishCar);

        wish.setAppStatus(ApplicationStatus.APPROVED);
        wishlistRepository.update(wish);
        applicationContext.publishEvent(new WishGrantedEvent(wish));
    }
}
