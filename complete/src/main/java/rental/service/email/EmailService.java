package rental.service.email;

import com.sendgrid.SendGrid;
import com.sendgrid.SendGridException;
import org.apache.commons.io.IOUtils;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.crypto.PlainText;
import org.owasp.esapi.errors.EncryptionException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import rental.data.model.Application;
import rental.data.model.Car;
import rental.data.model.User;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.Instant;
import java.util.Base64;
import java.util.List;
import java.util.StringJoiner;

/**
 * @author Tarita Ana
 */
@Service
public class EmailService {

    public static final String COMPANY_MAIL = "ACME_rental@gmail.com";
    public static final String PASSWORD_RESET = "Password reset";
    public static final String USER_REPORT = "applications.txt";

    @Value("${sendgrid.apikey}")
    private String apiKey;

    @Value("${password.reset.salt}")
    private String salt;

    private SendGrid sendgrid;


    @PostConstruct
    void setup() {
        sendgrid = new SendGrid(apiKey);
    }

    public void prepareAndSendForgotPassEmail(User user, String serverName, int serverPort) throws EncryptionException, IOException {
        StringJoiner stringJoiner = new StringJoiner("|");
        stringJoiner.add(user.getUserName());
        stringJoiner.add(Instant.now().toString());
        stringJoiner.add(salt);

        byte[] encryptedKey = ESAPI.encryptor().encrypt(new PlainText(stringJoiner.toString())).asPortableSerializedByteArray();
        String encoded = URLEncoder.encode(Base64.getEncoder().encodeToString(encryptedKey));

        String body = "Click this link:\n http://" + serverName + ":" + serverPort +
                "/reset-password?key=" + encoded;
        sendEmail(user.getMail(), PASSWORD_RESET, body, null);
    }

    public void prepareAndSendUserReportEmail(String to, String subject, String body, List<Application> applications) throws IOException {
        StringBuilder builder = new StringBuilder();
        builder.append(Application.getFields());
        for (Application application : applications) {
            builder.append(application.getFieldsContent() + "\n");
        }
        InputStream stream = IOUtils.toInputStream(builder.toString(), String.valueOf(StandardCharsets.UTF_8));
        sendEmail(to, subject, body, stream);
    }

    public void prepareNotificationEmail(Car car, List<String> to) throws IOException {
        for (String email : to) {
            sendEmail(email, car.getMake() + " price update", "The price for your previously rented car has changed!", null);
        }
    }

    public void prepareWinnerNotificationEmail(Car car, String email) throws IOException {
        sendEmail(email, "Congratulations", "You won the auction for " + car.getMake() + " " + car.getModel() + ". Please call us to redeem your winnings.", null);
    }

    public void prepareAuctionNotificationEmail(Car car, List<String> to) throws IOException {
        for (String email : to) {
            sendEmail(email, "A new auction was added!", car.getMake() + " " + car.getModel() + " was added to the site. Start bidding now!", null);
        }
    }

    public void sendWishGrantedEmail(Car car, User to) throws IOException {
        sendEmail(to.getMail(), car.getMake() + " was added", "The car you requested has been added to our offer!", null);
    }

    private void sendEmail(String to, String subject, String body, InputStream attachment) throws IOException {
        SendGrid.Email email = new SendGrid.Email();
        email.addTo(to);
        email.setFrom(COMPANY_MAIL);
        email.setSubject(subject);
        email.setText(body);
        if (attachment != null) {
            email.addAttachment(USER_REPORT, attachment);
        }

        try {
            sendgrid.send(email);
        } catch (SendGridException e) {
            throw new EmailException("Failed to send email", e);
        }
    }
}
