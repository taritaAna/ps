package rental.service;

import org.springframework.context.ApplicationContext;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import rental.data.model.Auction;
import rental.event.AuctionEndedEvent;

import javax.annotation.Resource;
import java.time.Instant;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Component
public class ScheduledTasks {

    @Resource
    private ApplicationContext applicationContext;

    @Resource
    private AuctionService auctionService;

    @Scheduled(fixedDelay = 5000)
    public void doSomething() {
        List<Auction> auctions = auctionService.fetchAuctions();
        auctions.stream()
                .filter(auction -> auction.getEndDate().toInstant().isAfter(Instant.now()) && !auction.isAwarded())
                .forEach(auction -> {
                    applicationContext.publishEvent(new AuctionEndedEvent(auction));
                });
    }
}
