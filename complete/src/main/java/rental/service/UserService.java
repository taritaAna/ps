package rental.service;

import com.google.common.hash.Hashing;
import org.owasp.esapi.ESAPI;
import org.owasp.esapi.crypto.CipherText;
import org.owasp.esapi.errors.EncryptionException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rental.data.model.User;
import rental.repository.UserRepository;

import javax.annotation.Resource;
import java.nio.charset.Charset;
import java.util.Base64;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Service
@Transactional
public class UserService {

    @Resource
    private UserRepository userRepository;

    public List<User> fetchUsers() {
        return userRepository.fetchUsers();
    }

    public User fetch(final int id) {
        return userRepository.fetch(id);
    }

    public void hashPasswordAndUpdate(User user) {
        user.setPassword(Hashing.sha1().hashString(user.getPassword(), Charset.forName("UTF-8")).toString());
        userRepository.update(user);
    }

    public void update(User user) {
        userRepository.update(user);
    }

    public void add(User user) {
        user.setPassword(Hashing.sha1().hashString(user.getPassword(), Charset.forName("UTF-8")).toString());
        userRepository.add(user);
    }

    public void delete(int carId) {
        userRepository.delete(carId);
    }

    public User fetchByUsername(String username) {
        return userRepository.fetchByUsername(username);
    }

    public User fetchByUsernameOrEmail(String usernameOrEmail) {
        return userRepository.fetchByUsernameOrEmail(usernameOrEmail);
    }

    public User getUserByPasswordResetKey(String key) throws EncryptionException {
        byte[] keyAsBytes = Base64.getDecoder().decode(key);
        String decoded = ESAPI.encryptor().decrypt(CipherText.fromPortableSerializedBytes(keyAsBytes)).toString();

        String username = decoded.split("\\|")[0];
        return fetchByUsername(username);
    }

    public List<User> fetchByNotification() {
        return userRepository.fetchByNotification();
    }
}
