package rental.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import rental.data.model.*;
import rental.data.model.util.ApplicationType;
import rental.data.model.util.PaymentStatus;
import rental.repository.ApplicationRepository;

import javax.annotation.Resource;
import java.sql.Date;
import java.util.List;

/**
 * @author Tarita Ana
 */
@Service
@Transactional
public class ApplicationService {

    @Resource
    private ApplicationRepository applicationRepository;

    @Resource
    private CarService carService;

    @Resource
    private UserService userService;

    public List<Application> fetchStandardApplications() {
        return applicationRepository.fetchStandardApplications();
    }

    public List<Application> fetchApplicationsByUser(String username) {
        return applicationRepository.fetchApplicationsByUser(username);
    }

    public Application fetch(final int id) {
        return applicationRepository.fetch(id);
    }

    public void update(Application application) {
        applicationRepository.update(application);
    }

    public Application canApprove(int appId) {
        Application application = fetch(appId);
        Car car = application.getCar();
        Date startDate = application.getStartDate();
        Date endDate = application.getEndDate();
        ApplicationType type = application.getType();
        List<Application> applications = fetchApprovedApplicationsForCar(type, car.getId(), startDate, endDate);
        if (applications == null || applications.isEmpty()) {
            application.setIsApproved(true);
        } else {
            application.setIsApproved(false);
        }
        return application;
    }

    public void add(ApplicationType type, int carId, String username, Date startDate, Date endDate) {
        Car car = carService.fetch(carId);
        User user = userService.fetchByUsername(username);

        Application application = new Application();
        application.setType(type);
        application.setCar(car);
        application.setStartDate(startDate);
        application.setEndDate(endDate);
        application.setUser(user);

        applicationRepository.add(application);
    }

    public void delete(int appId) {
        applicationRepository.delete(appId);
    }

    public List<Application> fetchApprovedApplicationsForCar(ApplicationType type, int id, Date startDate, Date endDate) {
        return applicationRepository.fetchApprovedApplicationsForCar(type, id, startDate, endDate);
    }

    public Application canCancel(int appId) {
        Application application = fetch(appId);
        if (!application.getPaymentStatus().equals(PaymentStatus.SUCCESS)) {
            application.setIsApproved(false);
        }
        return application;
    }
}
