package rental.event.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import rental.data.model.Auction;
import rental.event.AuctionEndedEvent;
import rental.service.AuctionService;
import rental.service.email.EmailService;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tarita Ana
 */
@Component
public class AuctionEndedEventListener implements ApplicationListener<AuctionEndedEvent> {
    private static final Logger LOGGER = Logger.getLogger(AuctionEndedEvent.class.getName());

    @Resource
    private EmailService emailService;

    @Resource
    private AuctionService auctionService;

    @Override
    public void onApplicationEvent(AuctionEndedEvent event) {
        try {
            Auction auction = event.getAuction();
            String to = auction.getLastBid().getUser().getMail();
            emailService.prepareWinnerNotificationEmail(auction.getCar(), to);
            auction.setAwarded(true);
            auctionService.update(auction);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Failed to send mail", e);
        }
    }
}
