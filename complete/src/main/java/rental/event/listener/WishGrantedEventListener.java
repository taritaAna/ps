package rental.event.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import rental.event.WishGrantedEvent;
import rental.service.email.EmailService;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Tarita Ana
 */
@Component
public class WishGrantedEventListener implements ApplicationListener<WishGrantedEvent> {
    private static final Logger LOGGER = Logger.getLogger(WishGrantedEvent.class.getName());

    @Resource
    private EmailService emailService;

    @Override
    public void onApplicationEvent(WishGrantedEvent event) {
        try {
            emailService.sendWishGrantedEmail(event.getWishlist().getCar(), event.getWishlist().getUser());
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Failed to send mail", e);
        }
    }
}
