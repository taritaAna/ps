package rental.event.listener;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;
import rental.data.model.User;
import rental.event.AuctionAddedEvent;
import rental.service.UserService;
import rental.service.email.EmailService;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * @author Tarita Ana
 */
@Component
public class AuctionAddedEventListener implements ApplicationListener<AuctionAddedEvent> {
    private static final Logger LOGGER = Logger.getLogger(AuctionAddedEvent.class.getName());

    @Resource
    private EmailService emailService;

    @Resource
    private UserService userService;

    @Override
    public void onApplicationEvent(AuctionAddedEvent event) {
        try {
            List<String> emails = userService.fetchByNotification().stream()
                    .map(User::getMail)
                    .collect(Collectors.toList());
            emailService.prepareAuctionNotificationEmail(event.getAuction().getCar(), emails);
        } catch (IOException e) {
            LOGGER.log(Level.WARNING, "Failed to send mail", e);
        }
    }
}
