package rental.event;

import org.springframework.context.ApplicationEvent;
import rental.data.model.Wishlist;

/**
 * @author Tarita Ana
 */
public class WishGrantedEvent extends ApplicationEvent {
    private final Wishlist wishlist;


    public WishGrantedEvent(Wishlist wishlist) {
        super(wishlist);
        this.wishlist = wishlist;
    }

    public Wishlist getWishlist() {
        return wishlist;
    }
}
