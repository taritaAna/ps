package rental.event;

import org.springframework.context.ApplicationEvent;
import rental.data.model.Auction;

/**
 * @author Tarita Ana
 */
public class AuctionEndedEvent extends ApplicationEvent {
    private final Auction auction;


    public AuctionEndedEvent(Auction auction) {
        super(auction);
        this.auction = auction;
    }

    public Auction getAuction() {
        return auction;
    }
}
