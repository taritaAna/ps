package rental;

import com.google.common.hash.Hashing;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import rental.data.model.Application;
import rental.data.model.*;
import rental.data.model.util.ApplicationType;
import rental.data.model.util.CarType;
import rental.data.model.util.FuelType;
import rental.data.model.util.Role;

import javax.annotation.Resource;
import java.nio.charset.Charset;
import java.sql.Date;
import java.util.Arrays;
import java.util.HashSet;

@Controller
public class InitController {

    @Resource
    private SessionFactory sessionFactory;

    @RequestMapping("/init")
    public String greeting(@RequestParam(value = "name", required = false, defaultValue = "World") String name, Model model) {
        model.addAttribute("name", name);
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();

        Car car = new Car();
        car.setMake("Audi");
        car.setModel("A4");
        car.setYear((short) 2004);
        car.setFuelType(FuelType.GASOLINE);
        car.setLocation("Cluj-Napoca");
        car.setMileage(205_000);
        car.setRentalPrice(50);
        car.setCarType(CarType.STANDARD);

        User user = new User();
        user.setFirstName("The");
        user.setLastName("GOD");
        user.setMail("admin@god.com");
        user.setNationalIdentifier("9569595926565");
        user.setPassword(Hashing.sha1().hashString("admin", Charset.forName("UTF-8")).toString());
        user.setRole(Role.ADMIN);
        user.setUserName("admin");

        Application application = new Application();
        application.setCar(car);
        application.setStartDate(new Date(new java.util.Date().getTime()));
        application.setEndDate(new Date(new java.util.Date().getTime()));
        application.setUser(user);
        application.setType(ApplicationType.RENTAL);
        car.setApplications(new HashSet<>(Arrays.asList(application)));

        session.persist(car);
        session.persist(user);
        session.persist(application);
        transaction.commit();

        session.close();

        session = sessionFactory.openSession();
        session.close();

        return "index";
    }

}
